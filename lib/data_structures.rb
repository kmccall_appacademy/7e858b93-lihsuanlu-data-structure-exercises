# EASY

# Write a method that returns the range of its argument (an array of integers).
def range(arr)
  # your code goes here
  arr.sort.reverse[0]-arr.sort[0]
end

# Write a method that returns a boolean indicating whether an array is in sorted
# order. Use the equality operator (==), which returns a boolean indicating
# whether its operands are equal, e.g., 2 == 2 => true, ["cat", "dog"] ==
# ["dog", "cat"] => false
def in_order?(arr)
  # your code goes here
  arr == arr.sort
end


# MEDIUM

# Write a method that returns the number of vowels in its argument
def num_vowels(str)
  # your code goes here
  str = str.downcase
  vowels = ['a', 'i', 'e', 'o', 'u']
  ans = 0
  str.chars.each do |letter|
    if vowels.include?(letter)
      ans += 1
    end
  end
  ans
end

# Write a method that returns its argument with all its vowels removed.
def devowel(str)
  # your code goes here
  vowels = ['a', 'i', 'e', 'o', 'u','A','E','I','O','U']
  str = str.chars
  str.map! do |letter|
    if vowels.include?(letter)
      letter = ''
    else
      letter = letter
    end
  end
  str.join
end


# HARD

# Write a method that returns the returns an array of the digits of a
# non-negative integer in descending order and as strings, e.g.,
# descending_digits(4291) #=> ["9", "4", "2", "1"]
def descending_digits(int)
  # your code goes here
  array_of_digits = []
  until int/10 == 0
    array_of_digits << (int%10).to_s
    int /= 10
  end
  array_of_digits << (int%10).to_s
  array_of_digits.sort.reverse
end

# Write a method that returns a boolean indicating whether a string has
# repeating letters. Capital letters count as repeats of lowercase ones, e.g.,
# repeating_letters?("Aa") => true
def repeating_letters?(str)
  # your code goes here
  str = str.downcase.chars
  str.each_index do |index|
    if index < str.length-1
      if str[index] == str[index+1]
        return true
      end
    end
  end
  false
end

# Write a method that converts an array of ten integers into a phone number in
# the format "(123) 456-7890".
def to_phone_number(arr)
  # your code goes here
  arr = arr.join
  '(' + arr[0..2].to_s + ') ' + arr[3..5].to_s + '-' + arr[6..9]
end

# Write a method that returns the range of a string of comma-separated integers,
# e.g., str_range("4,1,8") #=> 7
def str_range(str)
  # your code goes here
  str = str.split(',')
  str.map!{|num| num.to_i}
  str.sort.reverse[0]-str.sort[0]
end


#EXPERT

# Write a method that is functionally equivalent to the rotate(offset) method of
# arrays. offset=1 ensures that the value of offset is 1 if no argument is
# provided. HINT: use the take(num) and drop(num) methods. You won't need much
# code, but the solution is tricky!
def my_rotate(arr, offset=1)
  # your code goes here
  lengthz = arr.length
  if offset > lengthz-1
    offset = offset%lengthz
  elsif offset < -lengthz+1
    offset = -offset%4
  end

  if offset == 0
    return arr
  elsif offset > 0
    return arr.drop(offset) + arr.take(offset)
  else
    return arr.drop(lengthz+offset) + arr.take(lengthz+offset)
  end
end
